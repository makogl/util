import os
import csv
import glob

def catcsv():
    for count, file in enumerate(files):
        print(count)
        print(files)
        if count==0:
            with open(file, encoding="shift-jis") as csv_file:
                f2 = csv.reader(csv_file)
                header = f2.__next__()
                dataWriter.writerow(header)
                
                for row in f2:
                    dataWriter.writerow(row)
        else:
            with open(file, encoding="shift-jis") as csv_file:
                f3 = csv.reader(csv_file)
                header = next(f3)
                
                for row in f3:
                    dataWriter.writerow(row)

def main():
    global files
    global dataWriter
    
    folder_path = os.path.dirname(os.path.abspath(__file__))
    files = glob.glob(folder_path + "\*.csv")
    lists = []

    f1 = open('out.csv', 'w', encoding='shift-jis', newline='')
    dataWriter = csv.writer(f1)
    
    catcsv()
    
    f1.close()

if __name__ == '__main__':
    main()
